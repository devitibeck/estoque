<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    protected $fillable = ['nome','id_fornecedor','valor_compra','valor_venda','quantidade'];
}
