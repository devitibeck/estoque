<nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item">
            <a class="nav-link" href="{{ route('dashboard') }}">
              <i class="ti-shield menu-icon"></i>
              <span class="menu-title">Dashboard</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('produto.index') }}">
              <i class="ti-package menu-icon"></i>
              <span class="menu-title">Produtos</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('fornecedor.index') }}">
              <i class="ti-truck menu-icon"></i>
              <span class="menu-title">Fornecedores</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('cliente.index') }}">
              <i class="ti-user menu-icon"></i>
              <span class="menu-title">Clientes</span>
            </a>
          </li>
          <hr>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('usuario.index') }}">
              <i class="ti-hand-open menu-icon"></i>
              <span class="menu-title">Usuarios</span>
            </a>
          </li>
        </ul>
      </nav>
