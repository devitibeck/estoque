@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Fornecedores</h4>
                <a href="{{ route('fornecedor.create') }}" class="btn btn-primary">Cadastrar</a>
                @if($fornecedores->count())
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>CNPJ</th>
                                <th>E-mail</th>
                                <th>Contato</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($fornecedores as $fornecedor)
                            <tr>
                                <td class="py-1">
                                    {{ $fornecedor->nome }}
                                </td>
                                <td>
                                    {{ $fornecedor->cnpj }}
                                </td>
                                <td>
                                    {{ $fornecedor->email }}
                                </td>
                                <td>
                                    {{ $fornecedor->contato }}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @else
                <h5 class="text-muted my-3">Sem fornecedores para mostrar.</h5>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
