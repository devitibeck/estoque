@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Clientes</h4>
                <a href="{{ route('cliente.create') }}" class="btn btn-primary">Cadastrar</a>
                @if($clientes->count())
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>CNPJ</th>
                                <th>E-mail</th>
                                <th>Contato</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($clientes as $cliente)
                            <tr>
                                <td class="py-1">
                                    {{ $cliente->nome }}
                                </td>
                                <td>
                                    {{ $cliente->cnpj }}
                                </td>
                                <td>
                                    {{ $cliente->email }}
                                </td>
                                <td>
                                    {{ $cliente->contato }}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @else
                <h5 class="text-muted my-3">Sem clientes para mostrar.</h5>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
