@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">CADASTRO DE CLIENTES</h4>
                    <form action="{{ route('cliente.store') }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col form-group">
                                <label for="nome">Nome</label>
                                <input name="nome" id="nome" type="text" class="form-control" placeholder="Nome do produto">
                            </div>
                            <div class="col-3 form-group">
                                <label for="cnpj">CNPJ</label>
                                <input name="cnpj" id="cnpj" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col form-group">
                                <label for="email">E-mail</label>
                                <input name="email" id="email" type="text" class="form-control" placeholder="Nome do produto">
                            </div>
                            <div class="col-4 form-group">
                                <label for="contato">Contato</label>
                                <input name="contato" id="contato" type="tel" class="form-control" placeholder="Telefone de contato">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <button type="submit" class="btn btn-success">Cadastrar</button>
                                <a href="{{ route('cliente.index') }}" class="btn btn-warning">Cancelar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
