@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Produtos</h4>
                <a href="{{ route('produto.create') }}" class="btn btn-primary">Cadastrar</a>
                @if($produtos->count())
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Fornecedor</th>
                                <th>V.Compra</th>
                                <th>V.Venda</th>
                                <th>Quantidade</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($produtos as $produto)
                            <tr>
                                <td class="py-1">
                                    {{ $produto->nome }}
                                </td>
                                <td>
                                    {{ $produto->fornecedor }}
                                </td>
                                <td>
                                    {{ $produto->valor_compra }}
                                </td>
                                <td>
                                    {{ $produto->valor_venda }}
                                </td>
                                <td>
                                    {{ $produto->quantidade }}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @else
                <h5 class="text-muted my-3">Sem produtos para mostrar.</h5>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
