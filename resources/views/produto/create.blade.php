@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Cadastro de produtos</h4>
                    <form action="{{ route('produto.store') }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col form-group">
                                <label for="nome">Nome</label>
                                <input name="nome" id="nome" type="text" class="form-control" placeholder="Nome do produto">
                            </div>
                            <div class="col-3 form-group">
                                <label for="id_fornecedor">Fornecedor</label>
                                <select name="id_fornecedor" id="id_fornecedor" type="text" class="form-control">
                                    @foreach ($fornecedores as $fornecedor)
                                        <option value="{{ $fornecedor->id }}">{{ $fornecedor->nome }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col form-group">
                                <label for="valor_compra">Valor de compra</label>
                                <input name="valor_compra" id="valor_compra" type="text" class="form-control" placeholder="Nome do produto">
                            </div>
                            <div class="col form-group">
                                <label for="valor_venda">Valor de venda</label>
                                <input name="valor_venda" id="valor_venda" type="text" class="form-control" placeholder="Nome do produto">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <button type="submit" class="btn btn-success">Cadastrar</button>
                                <a href="{{ route('produto.index') }}" class="btn btn-warning">Cancelar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
