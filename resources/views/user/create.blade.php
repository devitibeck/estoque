@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">CADASTRO DE USUARIOS</h4>
                    <form action="{{ route('usuario.store') }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col form-group">
                                <label for="name">Nome</label>
                                <input name="name" id="name" type="text" class="form-control" required>
                            </div>
                            <div class="col-3 form-group">
                                <label for="tipo">Tipo de acesso</label>
                                <select name="tipo" id="tipo" class="form-control">
                                    <option value="usuario">Usuario</option>
                                    <option value="administrador">Administrador</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col form-group">
                                <label for="email">Email</label>
                                <input name="email" id="email" type="email" class="form-control" required>
                            </div>
                            <div class="col-4 form-group">
                                <label for="cpf">CPF</label>
                                <input name="cpf" id="cpf" type="text" class="form-control" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col form-group">
                                <label for="password">Senha</label>
                                <input name="password" id="password" type="password" class="form-control" required>
                            </div>
                            <div class="col form-group">
                                <label for="confirma">Confirmar senha</label>
                                <input name="confirma" id="confirma" type="password" class="form-control" required>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col">
                                <button type="submit" class="btn btn-success">Cadastrar</button>
                                <a href="{{ route('usuario.index') }}" class="btn btn-warning">Cancelar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
