@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Usuarios</h4>
                <a href="{{ route('usuario.create') }}" class="btn btn-primary">Cadastrar</a>
                @if($usuarios->count())
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>CPF</th>
                                <th>E-mail</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($usuarios as $usuario)
                            <tr>
                                <td class="py-1">
                                    {{ $usuario->name }}
                                </td>
                                <td>
                                    {{ $usuario->cpf }}
                                </td>
                                <td>
                                    {{ $usuario->email }}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @else
                <h5 class="text-muted my-3">Sem produtos para mostrar.</h5>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
