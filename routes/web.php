<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['auth']], function () {

    // Route::group(['prefix' => 'admin'], function () {
    //     Route::get('/produto' , 'ProdutoController@index');
    // });

    // Dashboard
    Route::get('/dashboard' , "DashboardController@index")->name('dashboard');

    // Produto
    Route::get('/produto' , "ProdutoController@index")->name('produto.index');
    Route::get('/cadastro/produto' , "ProdutoController@create")->name('produto.create');
    Route::post('/produto/cadastrar' , 'ProdutoController@store')->name('produto.store');

    // Usuario
    Route::get('/usuarios' , 'UserController@index')->name('usuario.index');
    Route::get('/cadastro/usuario' , 'UserController@create')->name('usuario.create');
    Route::post('/usuario/cadastrar' , 'UserController@store')->name('usuario.store');

    // Fornecedor
    Route::get('/fornecedores' , 'FornecedorController@index')->name('fornecedor.index');
    Route::get('/cadastro/fornecedor' , 'FornecedorController@create')->name('fornecedor.create');
    Route::post('/fornecedor/cadastrar' , 'FornecedorController@store')->name('fornecedor.store');

    // Cliente
    Route::get('/clientes' , 'ClienteController@index')->name('cliente.index');
    Route::get('/cadastro/cliente' , 'ClienteController@create')->name('cliente.create');
    Route::post('/cliente/cadastrar' , 'ClienteController@store')->name('cliente.store');

});

Route::group(['middleware' => ['guest']], function () {
    Route::get('/', 'LoginController@index')->name('login');
});

// Autenticação
Route::post('/logar' , "LoginController@authenticate")->name("logar");
Route::get('/logout' , "LoginController@logout")->name("logout");
