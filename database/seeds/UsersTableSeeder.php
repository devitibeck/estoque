<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Patrick Barbosa',
            'email' => 'patrickbarbosa.dp@gmail.com',
            'cpf' => '165.132.387-90',
            'password' => bcrypt('123'),
        ]);
    }
}
